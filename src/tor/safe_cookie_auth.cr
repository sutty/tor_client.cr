# Copyright 2023 Cooperativa de Trabajo Sutty Ltda.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

require "random"
require "openssl/hmac"
require "./errors"

module Tor
  # Perform SAFECOOKIE authentication
  class SafeCookieAuth
    getter client : Client
    getter client_nonce : String

    # HMAC key
    KEY = "Tor safe cookie authentication controller-to-server hash"

    # Initialize with a Tor client instance
    def initialize(@client)
      @client_nonce = Random.new.hex(32).upcase
    end

    # Perform authentication by retrieving server nonce and proving
    # knowledge of the cookie's contents.
    def authenticate!
      responses = client.command("PROTOCOLINFO")
      auth = responses["AUTH"]
      cookie = control_auth_cookie auth.extra["COOKIEFILE"]

      unless auth.extra["METHODS"].split(",").includes? "SAFECOOKIE"
        raise Error.new("Tor instance doesn't support SAFECOOKIE.")
      end

      responses = client.command("AUTHCHALLENGE SAFECOOKIE #{client_nonce}")
      server_nonce = responses["AUTHCHALLENGE"].extra["SERVERNONCE"]

      responses = client.command("AUTHENTICATE #{hmac(cookie, client_nonce, server_nonce)}")

      if responses["OK"].nil?
        raise AuthenticationError.new("There was an error during authentication.")
      end

      nil
    end

    # Read the cookie file or fail if we don't have permission.
    private def control_auth_cookie(file : String) : String
      raise CookieNotReadableError.new("#{file} unreadable, check permissions.") unless File.readable? file

      File.read(file).to_slice.hexstring.upcase
    end

    # Generate an HMAC proof
    private def hmac(cookie : String, client_nonce : String, server_nonce : String) : String
      OpenSSL::HMAC.hexdigest(OpenSSL::Algorithm::SHA256, KEY, "#{cookie}#{client_nonce}#{server_nonce}".hexbytes).upcase
    end
  end
end
