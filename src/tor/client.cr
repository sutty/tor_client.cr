# Copyright 2023 Cooperativa de Trabajo Sutty Ltda.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

require "log"
require "socket"
require "./safe_cookie_auth"
require "./response"

module Tor
  class Client
    getter host : String
    getter port : UInt16
    getter sock : TCPSocket

    # Initialize the client with a host and port
    def initialize(@host, @port)
      @sock = TCPSocket.new(host, port)
    end

    # Perform safe cookie authentication, run this before anything else.
    def authenticate!
      SafeCookieAuth.new(self).authenticate!
    end

    # Send a command to Tor and read any responses.
    def command(line : String)
      Log.debug { line }

      sock << line
      sock << "\r\n"

      responses = Hash(String, Response).new

      loop do
        line = sock.gets

        break if line.nil?

        response = Response.new(line)
        responses[response.reply] = response

        Log.debug { response }

        break unless response.ok?
        break if response.eoc?
      end

      responses
    end

    # Close the connection.
    def close
      return if sock.closed?

      command "QUIT"
      sock.close
    end
  end
end
