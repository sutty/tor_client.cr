# Copyright 2023 Cooperativa de Trabajo Sutty Ltda.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

module Tor
  # Parses server responses.
  class Response
    OK = "250"
    SEPARATOR = /[ \-]/

    getter raw : String
    getter status : String
    getter content : String
    getter reply : String
    getter extra : Hash(String, String)

    # Initialize with the original string sent to the client
    def initialize(@raw)
      @extra = Hash(String, String).new
      @status = ""
      @content = ""
      @reply = ""
      parse
    end

    # Parse the string to obtain status, reply and any extra values
    def parse
      @status, @content = raw.split(SEPARATOR, 2)
      @reply, *extra = content.split(2)

      return if extra.empty?

      extra.first.split(" ").each do |e|
        key, *value = e.split("=", 2)

        if value.empty?
          @extra[reply] = key
        else
          @extra[key] = value.first.tr("\"", "")
        end
      end
    end

    # Did the command end correctly?
    def ok?
      status == OK
    end

    # Has the server stopped talking?
    def eoc?
      ok? && raw.starts_with?("#{status} ")
    end

    # For debugging
    def to_s
      raw
    end
  end
end
