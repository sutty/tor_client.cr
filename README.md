# Tor client

A minimal implementation of a [Tor Control
Protocol](https://torproject.gitlab.io/torspec/control-spec/) client.

It just does safe cookie authentication and lets you send commands and
get responses.

## Installation

1. Add the dependency to your `shard.yml`:

   ```yaml
   dependencies:
     tor_client:
       git: "https://0xacab.org/sutty/tor_client.cr"
   ```

2. Run `shards install`

## Usage

Since the only authentication mechanism implemented as of yet is
SAFECOOKIE, your program needs to have access to the `CookieAuthFile`
set on `torrc`.

```crystal
require "tor/client"

client = Tor::Client.new "127.0.0.1", 9051
client.authenticate!

client.command("SIGNAL RELOAD")
```

## Contributing

1. Fork it (<https://0xacab.org/sutty/tor_client.cr/-/forks/new>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [f](https://0xacab.org/fauno) - creator and maintainer
